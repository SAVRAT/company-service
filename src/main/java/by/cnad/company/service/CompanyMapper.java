package by.cnad.company.service;

import by.cnad.company.entity.Company;
import org.springframework.stereotype.Component;

@Component
public class CompanyMapper {

  public CompanyDto toDto(Company company) {
    return CompanyDto.builder()
        .id(company.getId())
        .currencyIso(company.getCurrencyIso())
        .name(company.getName())
        .status(company.getStatus())
        .vatNumber(company.getVatNumber())
        .build();
  }

}
