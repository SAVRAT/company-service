package by.cnad.company.service;

import by.cnad.company.repo.CompanyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CompanyService {

  private final CompanyRepository repository;
  private final CompanyMapper mapper;


  public CompanyDto getCompanyById(Long id) {
    return repository.findById(id)
        .map(mapper::toDto)
        .orElseThrow(()-> new ResourceNotFoundException("Company not found"));
  }
}
