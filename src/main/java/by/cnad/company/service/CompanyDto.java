package by.cnad.company.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDto {

  private Long id;
  private String name;
  private String status;
  private String vatNumber;
  private String currencyIso;
}
